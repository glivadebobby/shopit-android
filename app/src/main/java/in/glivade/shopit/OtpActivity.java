package in.glivade.shopit;

import static in.glivade.shopit.app.Activity.launchClearStack;
import static in.glivade.shopit.app.Api.KEY_EMAIL;
import static in.glivade.shopit.app.Api.KEY_FIRST_NAME;
import static in.glivade.shopit.app.Api.KEY_LAST_NAME;
import static in.glivade.shopit.app.Api.KEY_MESSAGE;
import static in.glivade.shopit.app.Api.KEY_OTP_CODE;
import static in.glivade.shopit.app.Api.KEY_PHONE;
import static in.glivade.shopit.app.Api.KEY_PROFILE_PICTURE;
import static in.glivade.shopit.app.Api.KEY_TOKEN;
import static in.glivade.shopit.app.Api.SEND_OTP;
import static in.glivade.shopit.app.Api.VERIFY_PHONE;
import static in.glivade.shopit.app.Constant.EXTRA_FORMAT;
import static in.glivade.shopit.app.Constant.EXTRA_PDUS;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsMessage;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import in.glivade.shopit.app.AppController;
import in.glivade.shopit.app.Constant;
import in.glivade.shopit.app.MyPreference;
import in.glivade.shopit.app.ToastBuilder;
import in.glivade.shopit.app.VolleyErrorHandler;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OtpActivity extends AppCompatActivity
        implements View.OnClickListener {

    private Context mContext;
    private TextInputEditText mEditTextOtp;
    private Button mButtonVerifyOtp;
    private TextView mTextViewResendOtp;
    private BroadcastReceiver mReceiverSms;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        initObjects();
        initCallbacks();
        setupBroadcastReceiver();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mReceiverSms, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mReceiverSms);
    }

    @Override
    public void onClick(View v) {
        if (v == mTextViewResendOtp) {
            showProgressDialog("Resending OTP..");
            sendOtp();
        } else if (v == mButtonVerifyOtp) {
            processOtp();
        }
    }

    private void initObjects() {
        mEditTextOtp = (TextInputEditText) findViewById(R.id.input_otp);
        mButtonVerifyOtp = (Button) findViewById(R.id.btn_verify_otp);
        mTextViewResendOtp = (TextView) findViewById(R.id.txt_resend_otp);

        mContext = this;
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(this);
    }

    private void initCallbacks() {
        mTextViewResendOtp.setOnClickListener(this);
        mButtonVerifyOtp.setOnClickListener(this);
    }

    private void setupBroadcastReceiver() {
        mReceiverSms = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                processSms(intent);
            }
        };
    }

    private void processSms(Intent intent) {
        final Bundle bundle = intent.getExtras();
        if (bundle != null) {
            final Object[] objPdus = (Object[]) bundle.get(EXTRA_PDUS);
            if (objPdus != null) {
                SmsMessage[] smsMsg = new SmsMessage[objPdus.length];
                for (int i = 0; i < objPdus.length; i++) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        String format = bundle.getString(EXTRA_FORMAT);
                        smsMsg[i] = SmsMessage.createFromPdu((byte[]) objPdus[i], format);
                    } else {
                        smsMsg[i] = SmsMessage.createFromPdu((byte[]) objPdus[i]);
                    }
                    String phoneNumber = smsMsg[i].getDisplayOriginatingAddress();
                    String msg = smsMsg[i].getDisplayMessageBody();
                    if (phoneNumber.contains(Constant.OTP_SENDER_ID)) {
                        String otp = msg.substring(0, 6);
                        receiveSms(otp);
                        processOtp();
                    }
                }
            }
        }
    }

    private void receiveSms(String otp) {
        mEditTextOtp.getText().clear();
        mEditTextOtp.setText(otp);
        mEditTextOtp.setSelection(otp.length());
    }

    private void processOtp() {
        String otp = mEditTextOtp.getText().toString().trim();
        if (validateInput(otp)) {
            showProgressDialog("Verifying code..");
            verifyPhone(getOtpRequestJson(otp));
        }
    }

    private JSONObject getOtpRequestJson(String otp) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_PHONE, mPreference.getPhone());
            jsonObject.put(KEY_OTP_CODE, otp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private boolean validateInput(String otp) {
        if (otp.isEmpty()) {
            mEditTextOtp.requestFocus();
            mEditTextOtp.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "OTP Code"));
            return false;
        } else if (otp.length() < 4) {
            mEditTextOtp.requestFocus();
            mEditTextOtp.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_length),
                            "OTP Code", 4, "digits"));
            return false;
        }
        return true;
    }

    private void verifyPhone(JSONObject otpRequestJson) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                VERIFY_PHONE, otpRequestJson, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                hideProgressDialog();
                handleOtpResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                VolleyErrorHandler.handle(mContext, error);
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "verify_phone");
    }

    private void handleOtpResponse(JSONObject response) {
        try {
            String firstName = response.getString(KEY_FIRST_NAME);
            String lastName = response.getString(KEY_LAST_NAME);
            String email = response.getString(KEY_EMAIL);
            String phone = response.getString(KEY_PHONE);
            String profilePicture = response.getString(KEY_PROFILE_PICTURE);
            String token = response.getString(KEY_TOKEN);

            mPreference.setName(firstName + " " + lastName);
            mPreference.setEmail(email);
            mPreference.setPhone(phone);
            mPreference.setImage(profilePicture);
            mPreference.setToken(token);
            launchClearStack(mContext, SplashActivity.class);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private JSONObject getSendOtpRequestJson() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_PHONE, mPreference.getPhone());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void sendOtp() {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                SEND_OTP, getSendOtpRequestJson(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                hideProgressDialog();
                handleSendOtpResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                VolleyErrorHandler.handle(mContext, error);
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "send_otp");
    }

    private void handleSendOtpResponse(JSONObject response) {
        try {
            String detail = response.getString(KEY_MESSAGE);
            ToastBuilder.build(mContext, detail);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
