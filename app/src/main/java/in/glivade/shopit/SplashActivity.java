package in.glivade.shopit;

import static in.glivade.shopit.app.Activity.launch;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import in.glivade.shopit.app.MyPreference;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SplashActivity extends AppCompatActivity {

    private Context mContext;
    private MyPreference mPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initObjects();
        checkUserSession();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initObjects() {
        mContext = this;
        mPreference = new MyPreference(mContext);
    }

    private void checkUserSession() {
        if (mPreference.getToken() == null) {
            launch(mContext, WelcomeActivity.class);
        } else if (mPreference.getPreference()) {
            launch(mContext, MainActivity.class);
        } else {
            launch(mContext, PreferenceActivity.class);
        }
    }
}
