package in.glivade.shopit;

import static in.glivade.shopit.app.Activity.launchClearStack;
import static in.glivade.shopit.app.Activity.launchDialPad;
import static in.glivade.shopit.app.Api.FCM_TOKEN;
import static in.glivade.shopit.app.Api.KEY_DESCRIPTION;
import static in.glivade.shopit.app.Api.KEY_EMAIL;
import static in.glivade.shopit.app.Api.KEY_FCM_TOKEN;
import static in.glivade.shopit.app.Api.KEY_ID;
import static in.glivade.shopit.app.Api.KEY_IMAGE;
import static in.glivade.shopit.app.Api.KEY_IS_AVAILABLE;
import static in.glivade.shopit.app.Api.KEY_IS_WISHED;
import static in.glivade.shopit.app.Api.KEY_MESSAGE;
import static in.glivade.shopit.app.Api.KEY_NAME;
import static in.glivade.shopit.app.Api.KEY_PHONE;
import static in.glivade.shopit.app.Api.KEY_PRICE;
import static in.glivade.shopit.app.Api.KEY_PRODUCTS;
import static in.glivade.shopit.app.Api.KEY_PROMOTIONS;
import static in.glivade.shopit.app.Api.KEY_RATING;
import static in.glivade.shopit.app.Api.KEY_SHOPS;
import static in.glivade.shopit.app.Api.KEY_TITLE;
import static in.glivade.shopit.app.Api.PRODUCT_PREFERENCE;
import static in.glivade.shopit.app.Api.SIGN_OUT;
import static in.glivade.shopit.app.Api.WISH_LIST;
import static in.glivade.shopit.app.Constant.SUPPORT_PHONE;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import com.rd.PageIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.glivade.shopit.adapter.MyPagerAdapter;
import in.glivade.shopit.adapter.ProductAdapter;
import in.glivade.shopit.adapter.ShopAdapter;
import in.glivade.shopit.app.AppController;
import in.glivade.shopit.app.MyPreference;
import in.glivade.shopit.app.ToastBuilder;
import in.glivade.shopit.app.VolleyErrorHandler;
import in.glivade.shopit.model.ProductItem;
import in.glivade.shopit.model.ShopItem;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        ProductAdapter.ProductClickListener {

    private static final int SLIDE_DELAY = 3000;
    private Context mContext;
    private DrawerLayout mDrawerLayout;
    private Toolbar mToolbar;
    private ViewPager mPagerPromotion;
    private PageIndicatorView mIndicatorView;
    private LinearLayout mLayoutMain;
    private RecyclerView mRecyclerViewShop, mRecyclerViewProduct;
    private NavigationView mNavigationView;
    private SwipeRefreshLayout mRefreshLayout;
    private List<Fragment> mFragmentListPromotion;
    private List<ShopItem> mShopItemList;
    private List<ProductItem> mProductItemList;
    private ShopAdapter mShopAdapter;
    private ProductAdapter mProductAdapter;
    private MyPagerAdapter mPagerAdapterPromotion;
    private Handler mHandler;
    private Runnable mRunnable;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initObjects();
        initCallbacks();
        initToolbar();
        initPromotionSlider();
        setupViewPager();
        setupRecyclerView();
        initRefresh();
        updateFcmToken();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(Gravity.START);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        mDrawerLayout.closeDrawers();
        return onNavClick(item.getItemId());
    }

    @Override
    public void onWishClick(int id) {
        wish(id);
    }

    private void initObjects() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        mPagerPromotion = (ViewPager) findViewById(R.id.pager_promotion);
        mIndicatorView = (PageIndicatorView) findViewById(R.id.indicator);
        mLayoutMain = (LinearLayout) findViewById(R.id.main);
        mRecyclerViewShop = (RecyclerView) findViewById(R.id.shop_list);
        mRecyclerViewProduct = (RecyclerView) findViewById(R.id.product_list);
        mNavigationView = (NavigationView) findViewById(R.id.nav);

        mContext = this;
        mFragmentListPromotion = new ArrayList<>();
        mShopItemList = new ArrayList<>();
        mProductItemList = new ArrayList<>();
        mPagerAdapterPromotion = new MyPagerAdapter(getSupportFragmentManager(),
                mFragmentListPromotion);
        mShopAdapter = new ShopAdapter(mContext, mShopItemList);
        mProductAdapter = new ProductAdapter(mContext, mProductItemList, this);
        mHandler = new Handler();
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mNavigationView.setNavigationItemSelectedListener(this);
    }

    private void initRefresh() {
        mRefreshLayout.setColorSchemeResources(R.color.accent);
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPreference();
            }
        });
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(true);
                getPreference();
            }
        });
    }

    private void initPromotionSlider() {
        mRunnable = new Runnable() {
            @Override
            public void run() {
                int count = mFragmentListPromotion.size();
                int currentItem = mPagerPromotion.getCurrentItem();
                if (currentItem == count - 1) {
                    mPagerPromotion.setCurrentItem(0, true);
                } else if (currentItem < count) {
                    mPagerPromotion.setCurrentItem(currentItem + 1, true);
                }
                mHandler.postDelayed(mRunnable, SLIDE_DELAY);
            }
        };
        mRunnable.run();
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_menu);
        mNavigationView.setItemIconTintList(null);
    }

    private void setupViewPager() {
        mPagerPromotion.setAdapter(mPagerAdapterPromotion);
        mIndicatorView.setViewPager(mPagerPromotion);
    }

    private void setupRecyclerView() {
        mRecyclerViewShop.setLayoutManager(new GridLayoutManager(mContext, 2));
        mRecyclerViewShop.setAdapter(mShopAdapter);
        mRecyclerViewShop.setNestedScrollingEnabled(false);

        mRecyclerViewProduct.setLayoutManager(new GridLayoutManager(mContext, 2));
        mRecyclerViewProduct.setAdapter(mProductAdapter);
        mRecyclerViewProduct.setNestedScrollingEnabled(false);
    }

    private void getPreference() {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(PRODUCT_PREFERENCE, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mRefreshLayout.setRefreshing(false);
                        mLayoutMain.setVisibility(View.VISIBLE);
                        handlePreferenceResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mRefreshLayout.setRefreshing(false);
                VolleyErrorHandler.handle(mContext, error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", mPreference.getToken());
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "preference");
    }

    private void handlePreferenceResponse(JSONObject response) {
        try {
            mFragmentListPromotion.clear();
            JSONArray promotionsArray = response.getJSONArray(KEY_PROMOTIONS);
            for (int i = 0; i < promotionsArray.length(); i++) {
                JSONObject promotionObject = promotionsArray.getJSONObject(i);
                String title = promotionObject.getString(KEY_TITLE);
                String description = promotionObject.getString(KEY_DESCRIPTION);
                String image = promotionObject.getString(KEY_IMAGE);
                mFragmentListPromotion.add(
                        PromotionFragment.newInstance(image, title, description));
            }
            mPagerAdapterPromotion.notifyDataSetChanged();
            mPagerPromotion.setOffscreenPageLimit(mFragmentListPromotion.size());
            mIndicatorView.setCount(mFragmentListPromotion.size());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            mShopItemList.clear();
            JSONArray shopsArray = response.getJSONArray(KEY_SHOPS);
            for (int i = 0; i < shopsArray.length(); i++) {
                JSONObject shopObject = shopsArray.getJSONObject(i);
                int id = shopObject.getInt(KEY_ID);
                String name = shopObject.getString(KEY_NAME);
                String phone = shopObject.getString(KEY_PHONE);
                String email = shopObject.getString(KEY_EMAIL);
                String image = shopObject.getString(KEY_IMAGE);
                if (shopObject.isNull(KEY_IMAGE)) image = null;
                mShopItemList.add(new ShopItem(id, name, phone, email, image));
            }
            mShopAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            mProductItemList.clear();
            JSONArray productsArray = response.getJSONArray(KEY_PRODUCTS);
            for (int i = 0; i < productsArray.length(); i++) {
                JSONObject productObject = productsArray.getJSONObject(i);
                int id = productObject.getInt(KEY_ID);
                double price = productObject.getDouble(KEY_PRICE);
                double rating = 0;
                if (!productObject.isNull(KEY_RATING)) rating = productObject.getDouble(KEY_RATING);
                String name = productObject.getString(KEY_NAME);
                String image = null;
                if (!productObject.isNull(KEY_IMAGE)) {
                    JSONObject imageObject = productObject.getJSONObject(KEY_IMAGE);
                    if (!imageObject.isNull(KEY_IMAGE)) image = imageObject.getString(KEY_IMAGE);
                }
                boolean isAvailable = productObject.getBoolean(KEY_IS_AVAILABLE);
                boolean isWished = productObject.getBoolean(KEY_IS_WISHED);

                mProductItemList.add(
                        new ProductItem(id, price, rating, name, image, isAvailable, isWished));
            }
            mProductAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean onNavClick(int itemId) {
        switch (itemId) {
            case R.id.nav_home:
                return true;
            case R.id.nav_wish_list:
                return false;
            case R.id.nav_share_app:
                shareApp();
                return false;
            case R.id.nav_call_us_now:
                launchDialPad(mContext, SUPPORT_PHONE);
                return false;
            case R.id.nav_logout:
                promptLogoutDialog();
                return false;
            default:
                return false;
        }
    }

    private void shareApp() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT,
                getString(R.string.app_name) + " https://play.google.com/store/apps/details?id="
                        + getPackageName());
        startActivity(Intent.createChooser(shareIntent, "Share App"));
    }

    private void promptLogoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to Logout?");
        builder.setPositiveButton("Logout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showProgressDialog("Logging out..");
                logoutUser();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void logoutUser() {
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, SIGN_OUT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        handleLogoutResponse();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                VolleyErrorHandler.handle(mContext, error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", mPreference.getToken());
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "logout");
    }

    private void wish(final int id) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT,
                WISH_LIST + id, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                handleWishResponse(id, response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyErrorHandler.handle(mContext, error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", mPreference.getToken());
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "wish");
    }

    private void handleWishResponse(int id, JSONObject response) {
        try {
            String message = response.getString(KEY_MESSAGE);
            ToastBuilder.build(mContext, message);

            boolean isWished = response.getBoolean(KEY_IS_WISHED);
            for (int i = 0; i < mProductItemList.size(); i++) {
                ProductItem productItem = mProductItemList.get(i);
                if (productItem.getId() == id) {
                    productItem.setWished(isWished);
                    mProductAdapter.notifyItemChanged(i);
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleLogoutResponse() {
        mPreference.clearUser();
        mPreference.setTokenUploaded(false);
        ToastBuilder.build(mContext, "You\'ve logged out");
        launchClearStack(this, SplashActivity.class);
    }

    private void updateFcmToken() {
        if (!mPreference.isTokenUploaded()) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(KEY_FCM_TOKEN, mPreference.getFcmToken());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT,
                    FCM_TOKEN, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String message = response.getString(KEY_MESSAGE);
                        ToastBuilder.build(mContext, message);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mPreference.setTokenUploaded(true);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyErrorHandler.handle(mContext, error);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", mPreference.getToken());
                    return headers;
                }
            };

            AppController.getInstance().addToRequestQueue(jsonObjectRequest, "fcm_token");
        }
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
