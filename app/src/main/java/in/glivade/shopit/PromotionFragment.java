package in.glivade.shopit;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;


/**
 * A simple {@link Fragment} subclass.
 */
public class PromotionFragment extends Fragment {

    private static final String EXTRA_IMAGE = "image";
    private static final String EXTRA_TITLE = "title";
    private static final String EXTRA_DESCRIPTION = "description";
    private Context mContext;
    private View mRootView;
    private ImageView mImageViewPromotion;
    private TextView mTextViewTitle, mTextViewDescription;

    public PromotionFragment() {
        // Required empty public constructor
    }

    public static PromotionFragment newInstance(String image, String title, String description) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_IMAGE, image);
        bundle.putString(EXTRA_TITLE, title);
        bundle.putString(EXTRA_DESCRIPTION, description);

        PromotionFragment fragment = new PromotionFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_promotion, container, false);
        initObjects();
        prepareObjects();
        return mRootView;
    }

    private void initObjects() {
        mImageViewPromotion = (ImageView) mRootView.findViewById(R.id.img_promotion);
        mTextViewTitle = (TextView) mRootView.findViewById(R.id.txt_title);
        mTextViewDescription = (TextView) mRootView.findViewById(R.id.txt_description);

        mContext = getActivity();
    }

    private void prepareObjects() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(EXTRA_IMAGE)) {
                String image = getArguments().getString(EXTRA_IMAGE);
                if (image == null) {
                    Glide.with(mContext).load(R.drawable.placeholder).into(mImageViewPromotion);
                } else {
                    Glide.with(mContext).load(image).diskCacheStrategy(DiskCacheStrategy.ALL).into(
                            mImageViewPromotion);
                }
            }

            if (bundle.containsKey(EXTRA_TITLE)) {
                String title = getArguments().getString(EXTRA_TITLE);
                mTextViewTitle.setText(title);
            }

            if (bundle.containsKey(EXTRA_DESCRIPTION)) {
                String desc = getArguments().getString(EXTRA_DESCRIPTION);
                mTextViewDescription.setText(desc);
            }
        }
    }
}
