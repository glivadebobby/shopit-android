package in.glivade.shopit.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.glivade.shopit.R;

/**
 * Created by Bobby on 30-03-2017
 */

public class PreferenceHolder extends RecyclerView.ViewHolder {

    private ImageView mImageViewCategory, mImageViewPreferred;
    private TextView mTextViewCategory;

    public PreferenceHolder(View itemView) {
        super(itemView);
        mImageViewCategory = (ImageView) itemView.findViewById(R.id.img_category);
        mImageViewPreferred = (ImageView) itemView.findViewById(R.id.img_preferred);
        mTextViewCategory = (TextView) itemView.findViewById(R.id.txt_category);
    }

    public ImageView getImageViewCategory() {
        return mImageViewCategory;
    }

    public ImageView getImageViewPreferred() {
        return mImageViewPreferred;
    }

    public TextView getTextViewCategory() {
        return mTextViewCategory;
    }
}
