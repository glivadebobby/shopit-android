package in.glivade.shopit.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.glivade.shopit.R;

/**
 * Created by Bobby on 30-03-2017
 */

public class ShopHolder extends RecyclerView.ViewHolder {

    private ImageView mImageViewShop;
    private TextView mTextViewName;

    public ShopHolder(View itemView) {
        super(itemView);
        mImageViewShop = (ImageView) itemView.findViewById(R.id.img_shop);
        mTextViewName = (TextView) itemView.findViewById(R.id.txt_name);
    }

    public ImageView getImageViewShop() {
        return mImageViewShop;
    }

    public TextView getTextViewName() {
        return mTextViewName;
    }
}
