package in.glivade.shopit.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.glivade.shopit.R;

/**
 * Created by Bobby on 30-03-2017
 */

public class ProductHolder extends RecyclerView.ViewHolder {

    private ImageView mImageViewProduct, mImageViewWish;
    private TextView mTextViewName, mTextViewPrice;

    public ProductHolder(View itemView) {
        super(itemView);
        mImageViewProduct = (ImageView) itemView.findViewById(R.id.img_product);
        mImageViewWish = (ImageView) itemView.findViewById(R.id.img_wish);
        mTextViewName = (TextView) itemView.findViewById(R.id.txt_name);
        mTextViewPrice = (TextView) itemView.findViewById(R.id.txt_price);
    }

    public ImageView getImageViewProduct() {
        return mImageViewProduct;
    }

    public ImageView getImageViewWish() {
        return mImageViewWish;
    }

    public TextView getTextViewName() {
        return mTextViewName;
    }

    public TextView getTextViewPrice() {
        return mTextViewPrice;
    }
}
