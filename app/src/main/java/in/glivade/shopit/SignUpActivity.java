package in.glivade.shopit;

import static in.glivade.shopit.app.Activity.launch;
import static in.glivade.shopit.app.Api.KEY_EMAIL;
import static in.glivade.shopit.app.Api.KEY_FIRST_NAME;
import static in.glivade.shopit.app.Api.KEY_LAST_NAME;
import static in.glivade.shopit.app.Api.KEY_MESSAGE;
import static in.glivade.shopit.app.Api.KEY_PASSWORD;
import static in.glivade.shopit.app.Api.KEY_PHONE;
import static in.glivade.shopit.app.Api.SIGN_UP;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import in.glivade.shopit.app.AppController;
import in.glivade.shopit.app.MyPreference;
import in.glivade.shopit.app.ToastBuilder;
import in.glivade.shopit.app.VolleyErrorHandler;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignUpActivity extends AppCompatActivity
        implements View.OnClickListener {

    private static final int REQUEST_READ_SMS = 4;
    private Context mContext;
    private TextInputEditText mEditTextFirstName, mEditTextLastName, mEditTextEmail, mEditTextPhone,
            mEditTextPass;
    private Button mButtonSignUp;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initObjects();
        initCallbacks();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonSignUp) {
            processSignUp();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
            @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_SMS:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    processSignUp();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void initObjects() {
        mEditTextFirstName = (TextInputEditText) findViewById(R.id.input_first_name);
        mEditTextLastName = (TextInputEditText) findViewById(R.id.input_last_name);
        mEditTextEmail = (TextInputEditText) findViewById(R.id.input_email);
        mEditTextPhone = (TextInputEditText) findViewById(R.id.input_phone);
        mEditTextPass = (TextInputEditText) findViewById(R.id.input_pass);
        mButtonSignUp = (Button) findViewById(R.id.btn_sign_up);

        mContext = this;
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mButtonSignUp.setOnClickListener(this);
    }

    private void processSignUp() {
        String firstName = mEditTextFirstName.getText().toString().trim();
        String lastName = mEditTextLastName.getText().toString().trim();
        String email = mEditTextEmail.getText().toString().trim();
        String phone = mEditTextPhone.getText().toString().trim();
        String pass = mEditTextPass.getText().toString().trim();
        if (validateInput(firstName, lastName, email, phone, pass)) {
            if (hasReadSmsPermission()) {
                showProgressDialog("Signing up..");
                mPreference.setPhone(phone);
                signUp(getSignUpRequestJson(firstName, lastName, email, phone, pass));
            } else {
                requestReadSmsPermission();
            }
        }
    }

    private boolean validateInput(String firstName, String lastName, String email, String phone,
            String pass) {
        if (firstName.isEmpty()) {
            mEditTextFirstName.requestFocus();
            mEditTextFirstName.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "First Name"));
            return false;
        } else if (lastName.isEmpty()) {
            mEditTextLastName.requestFocus();
            mEditTextLastName.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Last Name"));
            return false;
        } else if (email.isEmpty()) {
            mEditTextEmail.requestFocus();
            mEditTextEmail.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty), "Email"));
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEditTextEmail.requestFocus();
            mEditTextEmail.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_invalid), "Email"));
            return false;
        } else if (phone.isEmpty()) {
            mEditTextPhone.requestFocus();
            mEditTextPhone.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Phone Number"));
            return false;
        } else if (phone.length() < 10) {
            mEditTextPhone.requestFocus();
            mEditTextPhone.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_length),
                            "Phone Number", 10, "digits"));
            return false;
        } else if (pass.isEmpty()) {
            mEditTextPass.requestFocus();
            mEditTextPass.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Password"));
            return false;
        } else if (pass.length() < 6) {
            mEditTextPass.requestFocus();
            mEditTextPass.setError(getString(R.string.error_pass_length));
            return false;
        }
        return true;
    }

    private JSONObject getSignUpRequestJson(String firstName, String lastName, String email,
            String phone, String pass) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_FIRST_NAME, firstName);
            jsonObject.put(KEY_LAST_NAME, lastName);
            jsonObject.put(KEY_EMAIL, email);
            jsonObject.put(KEY_PHONE, phone);
            jsonObject.put(KEY_PASSWORD, pass);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void signUp(JSONObject signUpRequestJson) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, SIGN_UP,
                signUpRequestJson, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                hideProgressDialog();
                try {
                    ToastBuilder.build(mContext, response.getString(KEY_MESSAGE));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                launch(mContext, OtpActivity.class);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                VolleyErrorHandler.handle(mContext, error);
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "sign_up");
    }

    private boolean hasReadSmsPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void requestReadSmsPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS},
                REQUEST_READ_SMS);
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
