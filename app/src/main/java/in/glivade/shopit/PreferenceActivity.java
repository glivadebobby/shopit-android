package in.glivade.shopit;

import static in.glivade.shopit.app.Activity.launchClearStack;
import static in.glivade.shopit.app.Api.FCM_TOKEN;
import static in.glivade.shopit.app.Api.KEY_CATEGORY;
import static in.glivade.shopit.app.Api.KEY_FCM_TOKEN;
import static in.glivade.shopit.app.Api.KEY_ID;
import static in.glivade.shopit.app.Api.KEY_IMAGE;
import static in.glivade.shopit.app.Api.KEY_IS_PREFERRED;
import static in.glivade.shopit.app.Api.KEY_MESSAGE;
import static in.glivade.shopit.app.Api.KEY_PREFERENCE;
import static in.glivade.shopit.app.Api.PREFERENCE;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.glivade.shopit.adapter.PreferenceAdapter;
import in.glivade.shopit.app.AppController;
import in.glivade.shopit.app.MyPreference;
import in.glivade.shopit.app.ToastBuilder;
import in.glivade.shopit.app.VolleyErrorHandler;
import in.glivade.shopit.model.PreferenceItem;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PreferenceActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;
    private Toolbar mToolbar;
    private SwipeRefreshLayout mRefreshLayout;
    private RecyclerView mRecyclerView;
    private FloatingActionButton mActionButtonDone;
    private List<PreferenceItem> mPreferenceItemList;
    private PreferenceAdapter mAdapter;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preference);
        initObjects();
        initToolbar();
        initCallbacks();
        initRecycler();
        initRefresh();
        updateFcmToken();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        if (v == mActionButtonDone) {
            showProgressDialog("Updating preference..");
            updatePreference();
        }
    }

    private void initObjects() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        mRecyclerView = (RecyclerView) findViewById(R.id.preference_list);
        mActionButtonDone = (FloatingActionButton) findViewById(R.id.fab_done);

        mContext = this;
        mPreferenceItemList = new ArrayList<>();
        mAdapter = new PreferenceAdapter(mContext, mPreferenceItemList);
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
    }

    private void initCallbacks() {
        mActionButtonDone.setOnClickListener(this);
    }

    private void initRecycler() {
        mRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initRefresh() {
        mRefreshLayout.setColorSchemeResources(R.color.accent);
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPreferences();
            }
        });
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(true);
                getPreferences();
            }
        });
    }

    private void getPreferences() {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(PREFERENCE,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        mRefreshLayout.setRefreshing(false);
                        mActionButtonDone.setVisibility(View.VISIBLE);
                        handlePreferenceResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mRefreshLayout.setRefreshing(false);
                VolleyErrorHandler.handle(mContext, error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", mPreference.getToken());
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonArrayRequest, "preference");
    }

    private void handlePreferenceResponse(JSONArray response) {
        mPreferenceItemList.clear();
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject preferenceObject = response.getJSONObject(i);
                int id = preferenceObject.getInt(KEY_ID);
                String category = preferenceObject.getString(KEY_CATEGORY);
                String image = preferenceObject.getString(KEY_IMAGE);
                boolean isPreferred = preferenceObject.getBoolean(KEY_IS_PREFERRED);
                mPreferenceItemList.add(i, new PreferenceItem(id, category, image, isPreferred));
                mAdapter.notifyItemInserted(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void updatePreference() {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        for (PreferenceItem preferenceItem : mPreferenceItemList) {
            if (preferenceItem.isPreferred()) jsonArray.put(preferenceItem.getId());
        }
        try {
            jsonObject.put(KEY_PREFERENCE, jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, PREFERENCE,
                jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                hideProgressDialog();
                handleUpdatePreferenceResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                VolleyErrorHandler.handle(mContext, error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", mPreference.getToken());
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "update_preference");
    }

    private void handleUpdatePreferenceResponse(JSONObject response) {
        try {
            String message = response.getString(KEY_MESSAGE);
            ToastBuilder.build(mContext, message);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mPreference.setPreference(true);
        launchClearStack(mContext, MainActivity.class);
    }

    private void updateFcmToken() {
        if (!mPreference.isTokenUploaded()) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(KEY_FCM_TOKEN, mPreference.getFcmToken());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT,
                    FCM_TOKEN, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    mPreference.setTokenUploaded(true);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyErrorHandler.handle(mContext, error);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", mPreference.getToken());
                    return headers;
                }
            };

            AppController.getInstance().addToRequestQueue(jsonObjectRequest, "fcm_token");
        }
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
