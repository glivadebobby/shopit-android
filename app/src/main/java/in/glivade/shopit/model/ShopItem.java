package in.glivade.shopit.model;

/**
 * Created by Bobby on 30-03-2017
 */

public class ShopItem {

    private int mId;
    private String mName, mPhone, mEmail, mImage;

    public ShopItem(int id, String name, String phone, String email, String image) {
        mId = id;
        mName = name;
        mPhone = phone;
        mEmail = email;
        mImage = image;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }
}
