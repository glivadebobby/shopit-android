package in.glivade.shopit.model;

/**
 * Created by Bobby on 30-03-2017
 */

public class PreferenceItem {

    private int mId;
    private String mCategory, mImage;
    private boolean mIsPreferred;

    public PreferenceItem(int id, String category, String image, boolean isPreferred) {
        mId = id;
        mCategory = category;
        mImage = image;
        mIsPreferred = isPreferred;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getCategory() {
        return mCategory;
    }

    public void setCategory(String category) {
        mCategory = category;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public boolean isPreferred() {
        return mIsPreferred;
    }

    public void setPreferred(boolean preferred) {
        mIsPreferred = preferred;
    }
}
