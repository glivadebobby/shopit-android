package in.glivade.shopit.model;

/**
 * Created by Bobby on 30-03-2017
 */

public class ProductItem {

    private int mId;
    private double mPrice, mRating;
    private String mName, mImage;
    private boolean mIsAvailable, mIsWished;

    public ProductItem(int id, double price, double rating, String name, String image,
            boolean isAvailable, boolean isWished) {
        mId = id;
        mPrice = price;
        mRating = rating;
        mName = name;
        mImage = image;
        mIsAvailable = isAvailable;
        mIsWished = isWished;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public double getPrice() {
        return mPrice;
    }

    public void setPrice(double price) {
        mPrice = price;
    }

    public double getRating() {
        return mRating;
    }

    public void setRating(double rating) {
        mRating = rating;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public boolean isAvailable() {
        return mIsAvailable;
    }

    public void setAvailable(boolean available) {
        mIsAvailable = available;
    }

    public boolean isWished() {
        return mIsWished;
    }

    public void setWished(boolean wished) {
        mIsWished = wished;
    }
}
