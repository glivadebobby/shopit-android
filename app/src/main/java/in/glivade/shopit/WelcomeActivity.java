package in.glivade.shopit;

import static in.glivade.shopit.app.Activity.launch;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class WelcomeActivity extends AppCompatActivity
        implements View.OnClickListener {

    private Context mContext;
    private Button mButtonSignIn, mButtonSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        initObjects();
        initCallbacks();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonSignIn) {
            launch(mContext, SignInActivity.class);
        } else if (v == mButtonSignUp) {
            launch(mContext, SignUpActivity.class);
        }
    }

    private void initObjects() {
        mButtonSignIn = (Button) findViewById(R.id.btn_sign_in);
        mButtonSignUp = (Button) findViewById(R.id.btn_sign_up);

        mContext = this;
    }

    private void initCallbacks() {
        mButtonSignIn.setOnClickListener(this);
        mButtonSignUp.setOnClickListener(this);
    }
}
