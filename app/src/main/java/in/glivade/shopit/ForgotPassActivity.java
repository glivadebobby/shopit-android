package in.glivade.shopit;

import static in.glivade.shopit.app.Activity.launch;
import static in.glivade.shopit.app.Api.KEY_MESSAGE;
import static in.glivade.shopit.app.Api.KEY_PHONE;
import static in.glivade.shopit.app.Api.SEND_OTP;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import in.glivade.shopit.app.AppController;
import in.glivade.shopit.app.MyPreference;
import in.glivade.shopit.app.ToastBuilder;
import in.glivade.shopit.app.VolleyErrorHandler;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ForgotPassActivity extends AppCompatActivity
        implements View.OnClickListener {

    private static final int REQUEST_READ_SMS = 4;
    private Context mContext;
    private TextInputEditText mEditTextPhone;
    private Button mButtonResetPass;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass);
        initObjects();
        initCallbacks();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
            @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_SMS:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    processSendOtp();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonResetPass) {
            processSendOtp();
        }
    }

    private void initObjects() {
        mEditTextPhone = (TextInputEditText) findViewById(R.id.input_phone);
        mButtonResetPass = (Button) findViewById(R.id.btn_reset_pass);

        mContext = this;
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mButtonResetPass.setOnClickListener(this);
    }

    private void processSendOtp() {
        String phone = mEditTextPhone.getText().toString().trim();
        if (validateInput(phone)) {
            if (hasReadSmsPermission()) {
                showProgressDialog("Verifying phone..");
                mPreference.setPhone(phone);
                sendOtp(getSendOtpRequestJson(phone));
            } else {
                requestReadSmsPermission();
            }
        }
    }

    private JSONObject getSendOtpRequestJson(String phone) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_PHONE, phone);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private boolean validateInput(String phone) {
        if (phone.isEmpty()) {
            mEditTextPhone.requestFocus();
            mEditTextPhone.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Phone Number"));
            return false;
        }
        return true;
    }

    private void sendOtp(JSONObject sendOtpRequestJson) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, SEND_OTP,
                sendOtpRequestJson, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                hideProgressDialog();
                handleSendOtpResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                VolleyErrorHandler.handle(mContext, error);
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "send_otp");
    }

    private void handleSendOtpResponse(JSONObject response) {
        try {
            String detail = response.getString(KEY_MESSAGE);
            ToastBuilder.build(mContext, detail);
            launch(mContext, SetPassActivity.class);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean hasReadSmsPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void requestReadSmsPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS},
                REQUEST_READ_SMS);
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
