package in.glivade.shopit.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;
import java.util.Locale;

import in.glivade.shopit.R;
import in.glivade.shopit.holder.ProductHolder;
import in.glivade.shopit.model.ProductItem;

/**
 * Created by Bobby on 30-03-2017
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductHolder> {

    private Context mContext;
    private List<ProductItem> mProductItemList;
    private ProductClickListener mListener;

    public ProductAdapter(Context context, List<ProductItem> productItemList,
            ProductClickListener listener) {
        mContext = context;
        mProductItemList = productItemList;
        mListener = listener;
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent,
                        false));
    }

    @Override
    public void onBindViewHolder(ProductHolder holder, int position) {
        ProductItem productItem = mProductItemList.get(position);
        final int id = productItem.getId();
        double price = productItem.getPrice();
        String name = productItem.getName();
        String image = productItem.getImage();
        boolean isWished = productItem.isWished();

        if (isWished) {
            holder.getImageViewWish().setImageResource(R.drawable.ic_wished);
        } else {
            holder.getImageViewWish().setImageResource(R.drawable.ic_wish);
        }

        if (image == null) {
            Glide.with(mContext).load(R.drawable.placeholder).diskCacheStrategy(
                    DiskCacheStrategy.ALL).into(holder.getImageViewProduct());
        } else {
            Glide.with(mContext).load(image).diskCacheStrategy(DiskCacheStrategy.ALL).into(
                    holder.getImageViewProduct());
        }

        holder.getTextViewName().setText(name);
        holder.getTextViewPrice().setText(
                String.format(Locale.getDefault(), mContext.getString(R.string.format_price),
                        price));
        holder.getImageViewWish().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onWishClick(id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mProductItemList.size();
    }

    public interface ProductClickListener {
        void onWishClick(int id);
    }
}
