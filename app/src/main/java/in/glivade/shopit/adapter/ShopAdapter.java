package in.glivade.shopit.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import in.glivade.shopit.R;
import in.glivade.shopit.holder.ShopHolder;
import in.glivade.shopit.model.ShopItem;

/**
 * Created by Bobby on 30-03-2017
 */

public class ShopAdapter extends RecyclerView.Adapter<ShopHolder> {

    private Context mContext;
    private List<ShopItem> mShopItemList;

    public ShopAdapter(Context context, List<ShopItem> shopItemList) {
        mContext = context;
        mShopItemList = shopItemList;
    }

    @Override
    public ShopHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ShopHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shop, parent,
                        false));
    }

    @Override
    public void onBindViewHolder(ShopHolder holder, int position) {
        ShopItem shopItem = mShopItemList.get(position);
        int id = shopItem.getId();
        String name = shopItem.getName();
        String image = shopItem.getImage();

        holder.getTextViewName().setText(name);
        if (image == null) {
            Glide.with(mContext).load(R.drawable.placeholder).diskCacheStrategy(
                    DiskCacheStrategy.ALL).into(holder.getImageViewShop());
        } else {
            Glide.with(mContext).load(image).diskCacheStrategy(DiskCacheStrategy.ALL).into(
                    holder.getImageViewShop());
        }
    }

    @Override
    public int getItemCount() {
        return mShopItemList.size();
    }
}
