package in.glivade.shopit.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import in.glivade.shopit.R;
import in.glivade.shopit.holder.PreferenceHolder;
import in.glivade.shopit.model.PreferenceItem;

/**
 * Created by Bobby on 30-03-2017
 */

public class PreferenceAdapter extends RecyclerView.Adapter<PreferenceHolder> {

    private Context mContext;
    private List<PreferenceItem> mPreferenceItemList;

    public PreferenceAdapter(Context context, List<PreferenceItem> preferenceItemList) {
        mContext = context;
        mPreferenceItemList = preferenceItemList;
    }

    @Override
    public PreferenceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PreferenceHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_preference, parent,
                        false));
    }

    @Override
    public void onBindViewHolder(PreferenceHolder holder, int position) {
        final PreferenceItem preferenceItem = mPreferenceItemList.get(position);
        String category = preferenceItem.getCategory();
        String image = preferenceItem.getImage();
        boolean isPreferred = preferenceItem.isPreferred();

        if (isPreferred) {
            holder.getImageViewPreferred().setVisibility(View.VISIBLE);
        } else {
            holder.getImageViewPreferred().setVisibility(View.GONE);
        }

        holder.getTextViewCategory().setText(category);
        Glide.with(mContext).load(image).diskCacheStrategy(DiskCacheStrategy.ALL).into(
                holder.getImageViewCategory());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preferenceItem.setPreferred(!preferenceItem.isPreferred());
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPreferenceItemList.size();
    }
}
