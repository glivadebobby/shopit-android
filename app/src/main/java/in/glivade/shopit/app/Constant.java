package in.glivade.shopit.app;

/**
 * Created by Bobby on 30-03-2017
 */

public class Constant {
    /**
     * Bundle Keys
     */
    public static final String EXTRA_ID = "id";
    public static final String EXTRA_PDUS = "pdus";
    public static final String EXTRA_FORMAT = "format";
    /**
     * OTP
     */
    public static final String OTP_SENDER_ID = "SHOPIT";
    /**
     * Call us now
     */
    public static final String SUPPORT_PHONE = "9994611195";
}
