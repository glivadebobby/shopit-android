package in.glivade.shopit.app;

/**
 * Created by Bobby on 30-03-2017
 */

public class Api {
    /**
     * Api Keys
     */
    public static final String KEY_ID = "id";
    public static final String KEY_CATEGORY = "category";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_NAME = "name";
    public static final String KEY_FIRST_NAME = "first_name";
    public static final String KEY_LAST_NAME = "last_name";
    public static final String KEY_TOTAL_PRICE = "total_price";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_IMAGES = "images";
    public static final String KEY_IMAGE = "image";
    public static final String KEY_IMAGE_FILE = "image[]";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_PROFILE_PICTURE = "profile_picture";
    public static final String KEY_PRODUCT = "product";
    public static final String KEY_DELETE_IMAGE = "delete_image";
    public static final String KEY_QUANTITY = "quantity";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_TOKEN = "access_token";
    public static final String KEY_OTP_CODE = "otp_code";
    public static final String KEY_TOTAL = "total";
    public static final String KEY_DELIVERED_AT = "delivered_at";
    public static final String KET_CREATED_AT = "created_at";
    public static final String KEY_PRICE = "price";
    public static final String KEY_NEXT_PAGE_URL = "next_page_url";
    public static final String KEY_DATA = "data";
    public static final String KEY_TICKER = "ticker";
    public static final String KEY_TITLE = "title";
    public static final String KEY_USER = "user";
    public static final String KEY_ITEM_COUNT = "item_count";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_ACTUAL_PRICE = "actual_price";
    public static final String KEY_PURCHASE_COUNT = "purchase_count";
    public static final String KEY_PRODUCT_COUNT = "product_count";
    public static final String KEY_BASE_PRICE = "base_price";
    public static final String KEY_IS_DEAL = "is_deal";
    public static final String KEY_METHOD = "_method";
    public static final String KEY_DELIVERY_STATUS = "delivery_status";
    public static final String KEY_STATUS = "status";
    public static final String KEY_STATUS_ID = "status_id";
    public static final String KEY_CURRENT_PASS = "current_password";
    public static final String KEY_NEW_PASSWORD = "new_password";
    public static final String KEY_CART_COUNT = "cart_count";
    public static final String KEY_FLAT_NO = "flat_no";
    public static final String KEY_STREET = "street";
    public static final String KEY_AREA = "area";
    public static final String KEY_CITY = "city";
    public static final String KEY_STATE = "state";
    public static final String KEY_COUNTRY = "country";
    public static final String KEY_PIN_CODE = "pin_code";
    public static final String KEY_LANDMARK = "landmark";
    public static final String KEY_DOOR_NO = "door_no";
    public static final String KEY_SHARE = "share";
    public static final String KEY_SUPPORT = "support";
    public static final String KEY_ABOUT = "about";
    public static final String KEY_IS_PREFERRED = "is_preferred";
    public static final String KEY_PAYMENT_TYPES = "payment_types";
    public static final String KEY_PAYMENT_MODE = "payment_mode";
    public static final String KEY_PREFERENCE = "preference";
    public static final String KEY_PROMOTIONS = "promotions";
    public static final String KEY_SHOPS = "shops";
    public static final String KEY_PRODUCTS = "products";
    public static final String KEY_IS_AVAILABLE = "is_available";
    public static final String KEY_IS_WISHED = "is_wished";
    public static final String KEY_RATING = "rating";
    public static final String KEY_FCM_TOKEN = "fcm_token";
    public static final String KEY_SHIPPING_COST = "shipping_cost";
    /**
     * Api URLS
     */
    private static final String URL = "http://shopit.glivade.in/api/v1/";
    public static final String SIGN_IN = URL + "user/sign-in";
    public static final String SIGN_UP = URL + "user/sign-up";
    public static final String SIGN_OUT = URL + "user/sign-out";
    public static final String SEND_OTP = URL + "user/send-otp";
    public static final String VERIFY_PHONE = URL + "user/verify-phone";
    public static final String RESEND_OTP = URL + "user/resend-otp";
    public static final String CHANGE_PASS = URL + "user/change-password";
    public static final String UPDATE_PASS = URL + "user/update-password";
    public static final String FCM_TOKEN = URL + "user/fcm-token";
    public static final String SHOP = URL + "shop";
    public static final String CATEGORY = URL + "category";
    public static final String PRODUCT = URL + "product";
    public static final String PROMOTION = URL + "promotion";
    public static final String PREFERENCE = URL + "preference";
    public static final String PRODUCT_PREFERENCE = URL + "preference/product";
    public static final String WISH_LIST = URL + "wish-list/";
}
