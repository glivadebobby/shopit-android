package in.glivade.shopit.app;

import static in.glivade.shopit.app.Activity.launch;
import static in.glivade.shopit.app.Activity.launchClearStack;
import static in.glivade.shopit.app.Api.KEY_MESSAGE;

import android.content.Context;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import in.glivade.shopit.OtpActivity;
import in.glivade.shopit.R;
import in.glivade.shopit.SplashActivity;

/**
 * Created by Bobby on 28-03-2017
 */

public class VolleyErrorHandler {
    public static void handle(Context context, VolleyError volleyError) {
        if (volleyError instanceof NoConnectionError) {
            ToastBuilder.build(context, context.getString(R.string.error_no_internet));
        }

        MyPreference preference = new MyPreference(context);

        NetworkResponse networkResponse = volleyError.networkResponse;
        if (networkResponse != null) {
            int statusCode = networkResponse.statusCode;
            String error = new String(networkResponse.data);
            String message = null;
            try {
                JSONObject jsonObject = new JSONObject(error);
                if (jsonObject.has(KEY_MESSAGE)) {
                    message = jsonObject.getString(KEY_MESSAGE);
                } else {
                    for (int i = 0; i < jsonObject.length(); i++) {
                        JSONArray keyArray = new JSONArray(
                                jsonObject.getString(jsonObject.names().getString(i)));
                        message = keyArray.getString(0);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            switch (statusCode) {
                case 400:
                    if (message != null) {
                        ToastBuilder.build(context, message);
                    }
                    break;
                case 401:
                    if (message != null) {
                        ToastBuilder.build(context, message);
                    }
                    preference.clearUser();
                    launchClearStack(context, SplashActivity.class);
                    break;
                case 403:
                    if (message != null) {
                        ToastBuilder.build(context, message);
                    }
                    preference.clearUser();
                    launchClearStack(context, SplashActivity.class);
                    break;
                case 404:
                    if (message != null) {
                        ToastBuilder.build(context, message);
                    }
                    break;
                case 406:
                    if (message != null) {
                        ToastBuilder.build(context, message);
                        launch(context, OtpActivity.class);
                    }
                    break;
                case 422:
                    if (message != null) {
                        ToastBuilder.build(context, message);
                    }
                    break;
                default:
                    ToastBuilder.build(context, context.getString(R.string.error_unexpected));
                    break;
            }
        }
    }
}
