package in.glivade.shopit.app;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Bobby on 24-03-2017
 */

public class ToastBuilder {
    public static void build(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
