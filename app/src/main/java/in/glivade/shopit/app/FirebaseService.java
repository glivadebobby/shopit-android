package in.glivade.shopit.app;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Bobby on 31-03-2017
 */

public class FirebaseService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();
        storeToken(token);
    }

    private void storeToken(String token) {
        MyPreference preference = new MyPreference(this);
        preference.setFcmToken(token);
        preference.setTokenUploaded(false);
    }
}
