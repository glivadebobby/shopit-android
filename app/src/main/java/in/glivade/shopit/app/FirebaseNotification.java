package in.glivade.shopit.app;

import static in.glivade.shopit.app.Api.KEY_DESCRIPTION;
import static in.glivade.shopit.app.Api.KEY_TICKER;
import static in.glivade.shopit.app.Api.KEY_TITLE;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

import in.glivade.shopit.MainActivity;
import in.glivade.shopit.R;

/**
 * Created by Bobby on 31-03-2017
 */

public class FirebaseNotification extends FirebaseMessagingService {

    private static final int REQUEST_NOTIFICATION = 4;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        JSONObject jsonObject = new JSONObject(remoteMessage.getData());
        String ticker = null;
        String title = null;
        String description = null;
        try {
            ticker = jsonObject.getString(KEY_TICKER);
            title = jsonObject.getString(KEY_TITLE);
            description = jsonObject.getString(KEY_DESCRIPTION);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (new MyPreference(this).getToken() != null) {
            displayNotification(ticker, title, description);
        }
    }

    private void displayNotification(String ticker, String title, String description) {
        Bitmap bitmapLargeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setTicker(ticker)
                .setContentTitle(title)
                .setContentText(description)
                .setSmallIcon(R.drawable.ic_notify)
                .setLargeIcon(bitmapLargeIcon)
                .setSound(defaultSoundUri)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setDefaults(Notification.DEFAULT_ALL);

        builder.setContentIntent(PendingIntent.getActivity(this, REQUEST_NOTIFICATION,
                new Intent(this, MainActivity.class), PendingIntent.FLAG_ONE_SHOT));

        Random random = new Random();
        int notificationId = random.nextInt(9999 - 1000) + 1000;

        NotificationManager notificationManager = (NotificationManager) getSystemService(
                Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationId, builder.build());
    }
}
