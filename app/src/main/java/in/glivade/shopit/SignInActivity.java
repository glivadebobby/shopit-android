package in.glivade.shopit;

import static in.glivade.shopit.app.Activity.launch;
import static in.glivade.shopit.app.Activity.launchClearStack;
import static in.glivade.shopit.app.Api.KEY_EMAIL;
import static in.glivade.shopit.app.Api.KEY_FIRST_NAME;
import static in.glivade.shopit.app.Api.KEY_LAST_NAME;
import static in.glivade.shopit.app.Api.KEY_PASSWORD;
import static in.glivade.shopit.app.Api.KEY_PHONE;
import static in.glivade.shopit.app.Api.KEY_PROFILE_PICTURE;
import static in.glivade.shopit.app.Api.KEY_TOKEN;
import static in.glivade.shopit.app.Api.SIGN_IN;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import in.glivade.shopit.app.AppController;
import in.glivade.shopit.app.MyPreference;
import in.glivade.shopit.app.VolleyErrorHandler;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignInActivity extends AppCompatActivity
        implements View.OnClickListener {

    private Context mContext;
    private TextInputEditText mEditTextPhone, mEditTextPass;
    private Button mButtonSignIn;
    private TextView mTextViewForgotPass;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        initObjects();
        initCallbacks();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == mTextViewForgotPass) {
            launch(mContext, ForgotPassActivity.class);
        } else if (v == mButtonSignIn) {
            processSignIn();
        }
    }

    private void initObjects() {
        mEditTextPhone = (TextInputEditText) findViewById(R.id.input_phone);
        mEditTextPass = (TextInputEditText) findViewById(R.id.input_pass);
        mButtonSignIn = (Button) findViewById(R.id.btn_sign_in);
        mTextViewForgotPass = (TextView) findViewById(R.id.txt_forgot_pass);

        mContext = this;
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mButtonSignIn.setOnClickListener(this);
        mTextViewForgotPass.setOnClickListener(this);
    }

    private void processSignIn() {
        String phone = mEditTextPhone.getText().toString().trim();
        String pass = mEditTextPass.getText().toString().trim();
        if (validateInput(phone, pass)) {
            mPreference.setPhone(phone);
            showProgressDialog("Signing in..");
            signIn(getSignInRequestJson(phone, pass));
        }
    }

    private JSONObject getSignInRequestJson(String phone, String pass) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_PHONE, phone);
            jsonObject.put(KEY_PASSWORD, pass);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private boolean validateInput(String phone, String pass) {
        if (TextUtils.isEmpty(phone)) {
            mEditTextPhone.requestFocus();
            mEditTextPhone.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Phone Number"));
            return false;
        } else if (TextUtils.isEmpty(pass)) {
            mEditTextPass.requestFocus();
            mEditTextPass.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty),
                            "Password"));
            return false;
        } else if (pass.length() < 6) {
            mEditTextPass.requestFocus();
            mEditTextPass.setError(getString(R.string.error_pass_length));
            return false;
        }
        return true;
    }

    private void signIn(JSONObject signInRequestJson) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, SIGN_IN,
                signInRequestJson, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                hideProgressDialog();
                handleSignInResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                VolleyErrorHandler.handle(mContext, error);
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "sign_in");
    }

    private void handleSignInResponse(JSONObject response) {
        try {
            String firstName = response.getString(KEY_FIRST_NAME);
            String lastName = response.getString(KEY_LAST_NAME);
            String email = response.getString(KEY_EMAIL);
            String phone = response.getString(KEY_PHONE);
            String profilePicture = response.getString(KEY_PROFILE_PICTURE);
            String token = response.getString(KEY_TOKEN);

            mPreference.setName(firstName + " " + lastName);
            mPreference.setEmail(email);
            mPreference.setPhone(phone);
            mPreference.setImage(profilePicture);
            mPreference.setToken(token);
            launchClearStack(mContext, SplashActivity.class);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
